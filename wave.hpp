#pragma once

int saveWave(const char* filename, const int format, const int sample_rate, const int numchnls,
             const int bitsPerSample, const unsigned char* data, const int sampleAmount)
{

	FILE* fp;
	fp = fopen(filename, "wb");
	if (!fp) {
		perror(NULL);
		return -1;
	}
	const int bytesPerSample = bitsPerSample / 8;

	// 4 write 'RIFF' =  0x52494646
	char fileId[4] = { 'R', 'I', 'F', 'F' };
	fwrite(fileId, sizeof(char), 4, fp);
	// 4 write ChunkSize = 4 + (8 + SubChunk1Size) + (8 + SubChunk2Size)
	unsigned int chnkSize = 4 + (36 + sampleAmount * bytesPerSample);
	fwrite(&chnkSize, 4, 1, fp);
	// 4 write 'WAVE' = 0x57415645
	char waveId[4] = { 'W', 'A', 'V', 'E' };
	fwrite(waveId, sizeof(char), 4, fp);

	// 4 write 'FMT' = 0x666d7420
	char fmtId[4] = { 'f', 'm', 't', ' ' };
	fwrite(fmtId, sizeof(char), 4, fp);
	// 4 write subchnk size = 16
	chnkSize = 16;
	fwrite(&chnkSize, 4, 1, fp);
	// 2 write AudioFormat = pcm = 1 ? float = 3?
	uint16_t t2 = format;
	fwrite(&t2, 2, 1, fp);
	// 2 write numchnls = 1 mono
	t2 = numchnls;
	fwrite(&t2, 2, 1, fp);
	// 4 write sampleRate = 44100
	chnkSize = sample_rate;
	fwrite(&chnkSize, 4, 1, fp);
	// 4 write byteRate = sampleRate * numchnls * bitspersample/8
	chnkSize = sample_rate * numchnls * bytesPerSample;
	fwrite(&chnkSize, 4, 1, fp);
	// 2 write blockalign = numchnls * bitspersample/8
	t2 = numchnls * bytesPerSample;
	fwrite(&t2, 2, 1, fp);
	// 2 write BitsPerSample = 32 float
	t2 = bitsPerSample;
	fwrite(&t2, 2, 1, fp);

	// 4 write Subchunk2ID
	// 'data' = 0x64617461
	char dataId[4] = { 'd', 'a', 't', 'a' };
	fwrite(dataId, sizeof(char), 4, fp);
	// 4 write
	// Subchunk2Size = NumSamples * NumChannels * BitsPerSample/8 write data
	chnkSize = sampleAmount * bytesPerSample;
	fwrite(&chnkSize, 4, 1, fp);
	// write samples
	fwrite(data, bytesPerSample, sampleAmount, fp);
	fclose(fp);
	return 1;
}
