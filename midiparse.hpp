// MIDI FORMAT REFERENCES
// https://www.csie.ntu.edu.tw/~r92092/ref/midi/
// http://www.music.mcgill.ca/~ich/classes/mumt306/StandardMIDIfileformat.html
// https://www.midi.org/specifications/item/general-midi

// https://en.wikipedia.org/wiki/General_MIDI
// https://en.wikipedia.org/wiki/General_MIDI_Level_2
// https://en.wikipedia.org/wiki/Comparison_of_MIDI_standards
#pragma once
#include "midiplayer.hpp"
#include <stdio.h>
#include <stdlib.h>

void debugPrintRange(const unsigned char* in, int i, int size)
{
	int s = MAX(0, i - 14);
	int e = MIN(i + 44, size);

	fprintf(stderr, "	");
	for (int h = s; h < e; h++) {
		if (h == i - 1) {
			fprintf(stderr, "  %hhx,  ", in[h]);
		} else {
			fprintf(stderr, "%hhx,", in[h]);
		}
	}
	fprintf(stderr, "\n");
}

// return amount read decoding
int varLen(uint8_t* in, uint32_t& out)
{
	int i = 0;
	// determin length of
	// number
	while ((in[i] & 0x80) != 0 && i < 4)
		i++;

	switch (i) {
	default:
	case 0:
		out = in[0];
		break;
	case 1:
		out = ((in[0] & 0x7f) << 7) | in[1];
		break;
	case 2:
		out = ((in[0] & 0x7f) << 14) | ((in[1] & 0x7f) << 7) | in[2];
		break;
	case 3:
		out = ((in[0] & 0x7f) << 24) | ((in[1] & 0x7f) << 14) | ((in[2] & 0x7f) << 7)
		    | in[3];
		break;
	}

	return i + 1;
}

union chnkDesc_t {
	char     name[4];
	uint32_t val;
	chnkDesc_t(){};
	chnkDesc_t(uint32_t v)
	    : val(v){};
	chnkDesc_t(const char n[4])
	    : name{ n[0], n[1], n[2], n[3] } {};
};
const chnkDesc_t MThd("MThd");
const chnkDesc_t MTrk("MTrk");

struct header_t {
	uint16_t format;
	uint16_t tracks;
	uint16_t division;

	bool seconds;
	// if false defines delta-time units in 'ticks per quarter note' 
	// if true defines delta-time in absolute terms (ticks/frame and frames/second)
	// bits 0-7 number of delta-time units per SMTPE frame
	// bits 8-14 form a negative number, representing the number of SMTPE frames per second

	void Convert()
	{
		format   = swapBits(format);
		tracks   = swapBits(tracks);
		division = swapBits(division) & 0x7fff;
		seconds  = division & 0x8000;
	}
};

inline int ParseMidi(const char* filename, song_t& song)
{
	FILE*          fp;
	long int       fsize;
	chnkDesc_t     chnkType;
	uint32_t       chnkSize;
	header_t       header;
	uint32_t       trkBuffSize       = 256;
	unsigned char* trkBuff           = (unsigned char*)malloc(trkBuffSize);
	int            rc                = 1;
	uint64_t       tempo             = 500000;
	const double   tempo_div         = double(tempo) / 120.0;
	const double   sample_rate       = 44100.0;
	const double   sample_per_minute = 60.0;
	double         divisions         = 256.0;
	double         tempo_h           = double(tempo) / tempo_div;
	double         ctimeMult = (sample_rate * sample_per_minute) / (divisions * tempo_h);

	bool unsupportedStatus = false;
	printf("ctimeMult %f\n", ctimeMult);

	fp = fopen(filename, "rb");
	if (fp == NULL) {
		fprintf(stderr, "file \"%s\" not found\n", filename);
		rc = -1;
		goto escape;
	}
	fseek(fp, 0, SEEK_END);
	fsize = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	while (fsize > 8) {
		fread(&chnkType, 4, 1, fp);
		fread(&chnkSize, 4, 1, fp);
		chnkSize = swapBits(chnkSize);

		printf("\n\n---chnk %.4s size %u tell %ld\n", chnkType.name, chnkSize, ftell(fp));

		if (chnkType.val == MThd.val) {

			fread(&header, sizeof(uint16_t) * 3, 1, fp);
			header.Convert();
			divisions = header.division;
			ctimeMult = (sample_rate * sample_per_minute) / (divisions * tempo_h);
			printf("	HEADER format=%u tracks=%u div=%u secs=%s\n", header.format,
			       header.tracks, header.division, header.seconds ? "true" : "false");
			printf("	ctimeMult %f\n", ctimeMult);

		} else if (chnkType.val == MTrk.val) {
			if (trkBuffSize < chnkSize) {
				trkBuffSize = trkBuffSize * 2;
				trkBuffSize = trkBuffSize > chnkSize ? trkBuffSize : chnkSize;
				trkBuff     = (unsigned char*)realloc(trkBuff, trkBuffSize);
			}
			track_t* track = new track_t;
			track->name = std::to_string(song.tracks.size());
			song.tracks.push_back(track);
			fread(trkBuff, chnkSize, 1, fp);
			uint32_t ctime   = 0;
			uint32_t dt      = 0;
			uint32_t length  = 0;
			uint8_t  status  = 0;
			uint8_t  ostatus = 0;
			uint8_t  mtype   = 0;
			int      r       = 0;
			uint8_t  ch      = 0;
			uint32_t filler  = 0;
			std::string str;

			while (r < chnkSize) {
				r += varLen((uint8_t*)(trkBuff + r), dt);

				ostatus = status;
				status  = trkBuff[r];
				r++;

				ctime += dt;
				switch (status) {

				// weird stuffs no documentation of meaning?
				case 0x00 ... 0x1f:
				case 0x20 ... 0x2f:
				case 0x30 ... 0x3f: // no idea
				case 0x40 ... 0x4f: // no idea
				case 0x50 ... 0x5f: // no idea
				case 0x60 ... 0x7f:
					unsupportedStatus = true;
					r += 1; // varLen((uint8_t*)(trkBuff + r),filler);
					break;
				// documented stuff
				case 0x80 ... 0x8f:
				case 0x90 ... 0x9f:


					// TODO
					// multiply ctime/detla time by some value so each increment
					// is equal to 44100hz
					ch = status & 0x0f;
					// if(ch == 10) // precussion event do something about that

					if (status & 0x10) {
						track->ch[ch].StartNote(trkBuff[r], trkBuff[r + 1],
						                       ctime * ctimeMult);
					} else {
						track->ch[ch].EndNote(trkBuff[r], trkBuff[r + 1],
						                      ctime * ctimeMult);
					}
					r += 2;
					break;
				case 0xa0 ... 0xaf: // poly key pressure
				case 0xb0 ... 0xbf: // controller change 
				case 0xe0 ... 0xef: // pitch bend
					r += 1;
				case 0xc0 ... 0xcf: // program change
				case 0xd0 ... 0xdf: // channel pressure
					r += 1;
					// printf("		VOICE MESSAGE\n");
					break;
				case 0xf0:
				case 0xf7:
					fprintf(stderr, "sysex events unssuported\n");
					r += varLen((uint8_t*)trkBuff + r, length);
					r += length;
					break;
				case 0xff:
					mtype = trkBuff[r];
					r++;
					//printf("		META EVENT %hhx\n", mtype);
					switch (mtype) {
					case 0x00:// sequence number
					case 0x01:// text
					case 0x02:// copyright
					case 0x03:// sequence/track name
					case 0x04:// instrument name
					case 0x05:// lyrics
					case 0x06:// marker
					case 0x07:// cue point

						// print whatever text is there to console
						r += varLen((uint8_t*)trkBuff + r, length);
						str = std::string((char*)trkBuff + r, 
								std::min(length, chnkSize -r ));
						switch(mtype) {	
						case 0x00:// sequence number
						case 0x01:// text
						case 0x02:// copyright
							printf("%s\n", str.c_str());
							break;
						case 0x03:// sequence/track name
							track->name = str;
							break;
						case 0x04:// instrument name
							track->name = str;
							break;
						case 0x05:// lyrics
							track->lyrics.push_back({ctime, str}); 
							break;
						case 0x06:// marker
						case 0x07:// cue point
							printf("%s\n", str.c_str());
						}

						r += length;
						break;
					case 0x20: // midi channel prefix
						r += 2;
						break;
					case 0x21: // NO IDEA ?
						r += 2;
						break;
					case 0x2f: // end of track
						r += 1;
						break;
					case 0x51: // set tempo
					{
						tempo = 
						      uint32_t(trkBuff[r + 1]) << 16
						    | uint32_t(trkBuff[r + 2]) << 8
						    | uint32_t(trkBuff[r + 3]);

						tempo_h   = double(tempo) / tempo_div;


						//ctimeMult = (sample_rate * sample_per_minute)
						//    / (divisions * tempo_h);

						double mul = (double(tempo) / divisions) / 1000.0;
	
						ctimeMult = mul * (double(SAMPLES) / 1000.0);

						// New tempo, in us/quarter-note
						printf("SET TEMPO @dt %u to %lu=%.03fbpm "
						       "ctimeMult=%.03f mul=%.03f\n",
						       ctime, tempo, tempo_h, ctimeMult, mul);
					}
						r += 4;
						break;
					case 0x54: // smtpe tempo
						r += 6;
						break;
					case 0x58: // time signature
						printf("SET TIME SIG=%hhu/%hhu CLOCK=%hhu "
						       "1/32NPC=%hhu\n",
						       trkBuff[r + 1], trkBuff[r + 2],
						       trkBuff[r + 3], trkBuff[r + 4]);
						r += 5;
						break;
					case 0x59: // key signature
						r += 3;
						break;
					case 0x7f: // sequence-specific meta event
						r += varLen((uint8_t*)trkBuff + r, length);
						r += length;
						break;
					default:
						fprintf(stderr, "	unsupported event %hhx\n",
						        mtype);
						debugPrintRange(trkBuff, r, trkBuffSize);
						rc = -1;
						goto escape;
					}
					break;
				default:

					fprintf(stderr,"	unsupported status type %hhx r=%d "
						"PREV type %hhx\n", status, r, ostatus);
					debugPrintRange(trkBuff, r, trkBuffSize);
					rc = -1;
					goto escape;
				}
			}
			size_t total_notes = 0;
			for (auto& chan : track->ch) {
				total_notes += chan.stack.size();
				if (chan.unresolved.size() > 0)
					fprintf(stderr, "TRACK HAS %zu UNRESOLVED NOTES\n",
					        chan.unresolved.size());
			}
			track->CalcLength();
			printf("TRACK LEN %u NOTES %zu\n", track->end, total_notes);
		} else {
			fprintf(stderr, "	unsupported chunk type %.4s\n", chnkType.name);
			fseek(fp, SEEK_CUR, chnkSize);
		}
		fsize -= 8;
		fsize -= chnkSize;
	};
escape:
	free(trkBuff);
	uint32_t notecount = 0;
	for (auto t : song.tracks) {
		for (auto ch : t->ch) {
			notecount += ch.stack.size();
		}
	}
	printf("SONG HAS %u NOTES\n", notecount);
	if (unsupportedStatus)
		fprintf(stderr, "SONG CONTAINS UNSUPPORTED STATUS TYPES\n");
	return rc;
}
