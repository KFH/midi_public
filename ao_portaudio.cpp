#include "ao.hpp"
#include <portaudio.h>
#include <stdio.h>
#include <string.h>

namespace ao {

struct dev_u {
	device_t      handle;
	PaDeviceIndex di;
};
static_assert(sizeof(device_t) >= sizeof(PaDeviceIndex), "DEVICE SIZE MISMATCH");

PaDeviceIndex GetDi(device_t& dev) { return ((dev_u*)&dev)->di; }
void          SetDi(device_t& dev, PaDeviceIndex di) { ((dev_u*)&dev)->di = di; }

int init()
{
	PaError err = Pa_Initialize();
	printf("Init portaudio "
	       "%s\n",
	       Pa_GetVersionText());
	return err == paNoError;
}

int open_device(device_t& dev)
{

	printf("DEVICES:\n");
	PaDeviceIndex count = Pa_GetDeviceCount();
	PaDeviceIndex pref  = paNoDevice;
	for (PaDeviceIndex i = 0; i < count; i++) {
		const PaDeviceInfo* di = Pa_GetDeviceInfo(i);
		if (di == nullptr)
			continue;
		printf("	%s\n", di->name);
		if (strcmp(di->name, "pulse") == 0)
			pref = i;
	}

	SetDi(dev, pref != paNoDevice ? pref : Pa_GetDefaultOutputDevice());

	// print stuff about
	// device
	const PaDeviceInfo* di = Pa_GetDeviceInfo(GetDi(dev));
	printf("SELECTED: %s @ %f \n", di->name, di->defaultSampleRate);

	return GetDi(dev) != paNoDevice;
}

int wrapCallback(const void* inputBuffer, void* outputBuffer, unsigned long framesPerBuffer,
                 const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags,
                 void* userData)
{
	(void)timeInfo; 
	(void)statusFlags;
	(void)inputBuffer;

	float*      out;
	streamCB_t* cb;
	out = (float*)outputBuffer;
	cb  = (streamCB_t*)userData;

	if (cb->Callback(out, framesPerBuffer))
		return paContinue;
	printf("STREAM EXITING\n");
	return paComplete;
}
int open_stream(device_t& dev, stream_t& stream, streamCB_t& cb)
{

	PaStream*                 pastream;
	const PaStreamParameters* inputParameters = nullptr;
	PaStreamParameters        outputParameters;
	PaTime                    sampleRate      = 44100;
	unsigned long             framesPerBuffer = 1024;
	PaStreamCallback*         streamCallback  = wrapCallback;
	void*                     userData        = &cb;
	PaError                   err;
	const PaDeviceInfo*       di = Pa_GetDeviceInfo(GetDi(dev));

	outputParameters.device       = GetDi(dev);
	outputParameters.channelCount = 1; /* stereo
	                                      output */
	outputParameters.sampleFormat = paFloat32; // paInt16;
	outputParameters.suggestedLatency
	    = Pa_GetDeviceInfo(outputParameters.device)->defaultLowOutputLatency * 1.5;
	outputParameters.hostApiSpecificStreamInfo = nullptr;

	if (Pa_IsFormatSupported(inputParameters, &outputParameters, sampleRate) != paNoError)
		sampleRate = di->defaultSampleRate;

	err = Pa_OpenStream(&pastream, inputParameters, &outputParameters, sampleRate,
	                    framesPerBuffer, paPrimeOutputBuffersUsingStreamCallback,
	                    streamCallback, userData);
	if (err != paNoError)
		return 0;

	stream = pastream;

	err = Pa_StartStream(pastream);

	return err == paNoError;
}

int state_stream(device_t dev, stream_t& stream)
{
	return Pa_IsStreamActive((PaStream*)stream) == 1;
}

int close_stream(device_t& dev, stream_t& stream)
{
	PaError err = Pa_CloseStream((PaStream*)stream);
	return err == paNoError;
}

int close_device(device_t& dev)
{
	// closing device
	// unnecessary
	return 1;
}

int quit()
{
	PaError err = Pa_Terminate();
	return err == paNoError;
};

} // namespace ao
