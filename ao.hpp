#pragma once
namespace ao {

typedef float aof_t;
typedef void* device_t;
typedef void* stream_t;

struct stream_info_t {
	double freq;
	int channels;
};

struct streamCB_t {
	// return 1 to continue 0 to exit
	virtual int Callback(float* in, int in_s) = 0;
};
// API should return 1 for sucess 0 or -1 to INT_MIN for failure codes
int init();
int open_device(device_t& dev);
int open_stream(device_t& dev, stream_t& stream, streamCB_t& cb);
int state_stream(device_t dev, stream_t& stream);
int close_stream(device_t& dev, stream_t& stream);
int close_device(device_t& dev);
int quit();
} // namespace ao
