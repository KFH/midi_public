#pragma once
// TODO
// on midi channel 10 each note corresponds to a drum sound need 46 samples
// for  drums or some synth equivalent noise
/*
35	Acoustic Bass Drum
36	Bass Drum 1
37	Side Stick
38	Acoustic Snare
39	Hand Clap
40	Electric Snare
41	Low Floor Tom
42	Closed Hi Hat
43	High Floor Tom
44	Pedal Hi-Hat
45	Low Tom
46	Open Hi-Hat
47	Low-Mid Tom
48	Hi-Mid Tom
49	Crash Cymbal 1
50	High Tom
51	Ride Cymbal 1
52	Chinese Cymbal
53	Ride Bell
54	Tambourine
55	Splash Cymbal
56	Cowbell
57 	Crash Cymbal 2 
58 	Vibraslap 
59	Ride Cymbal 2
60	Hi Bongo
61	Low Bongo
62	Mute Hi Conga
63	Open Hi Conga
64	Low Conga
65	High Timbale
66	Low Timbale
67	High Agogo68 Low Agogo
69	Cabasa
70	Maracas
71	Short Whistle
72	Long Whistle
73	Short Guiro
74	Long Guiro
75	Claves
76	Hi Wood Block
77	Low Wood Block
78	Mute Cuica
79	Open Cuica
80	Mute Triangle
81	Open Triangle
*/

struct drum_map_t {
	uint8_t keyout;
	uint8_t keyin;
	std::string name;
};

drum_map_t drum_mapping[82] = {
{200, 0  ,""},
{200, 1  ,""},
{200, 2  ,""},
{200, 3  ,""},
{200, 4  ,""},
{200, 5  ,""},
{200, 7  ,""},
{200, 8  ,""},
{200, 9  ,""},
{200, 10 ,""},
{200, 12 ,""},
{200, 13 ,""},
{200, 14 ,""},
{200, 15 ,""},
{200, 16 ,""},
{200, 17 ,""},
{200, 18 ,""},
{200, 19 ,""},
{200, 20 ,""},
{200, 21 ,""},
{200, 22 ,""},
{200, 23 ,""},
{200, 24 ,""},
{200, 25 ,""},
{200, 26 ,""},
{200, 27 ,""},
{200, 28 ,""},
{200, 29 ,""},
{200, 30 ,""},
{200, 31 ,""},
{200, 32 ,""},
{200, 33 ,""},
{200, 34 ,""},
{200, 35 ,"Acoustic Bass Drum"},
{200, 36 ,"Bass Drum 1"},
{200, 37 ,"Side Stick"},
{200, 38 ,"Acoustic Snare"},
{200, 39 ,"Hand Clap"},
{200, 40 ,"Electric Snare"},
{200, 41 ,"Low Floor Tom"},
{200, 42 ,"Closed Hi Hat"},
{200, 43 ,"High Floor Tom"},
{200, 44 ,"Pedal Hi-Hat"},
{200, 45 ,"Low Tom"},
{200, 46 ,"Open Hi-Hat"},
{200, 47 ,"Low-Mid Tom"},
{200, 48 ,"Hi-Mid Tom"},
{200, 49 ,"Crash Cymbal 1"},
{200, 50 ,"High Tom"},
{200, 51 ,"Ride Cymbal 1"},
{200, 52 ,"Chinese Cymbal"},
{200, 53 ,"Ride Bell"},
{200, 54 ,"Tambourine"},
{200, 55 ,"Splash Cymbal"},
{200, 56 ,"Cowbell"},
{200, 57 ,"Crash Cymbal 2"},
{200, 58 ,"Vibraslap"},
{200, 59 ,"Ride Cymbal 2"},
{200, 60 ,"Hi Bongo"},
{200, 61 ,"Low Bongo"},
{200, 62 ,"Mute Hi Conga"},
{200, 63 ,"Open Hi Conga"},
{200, 64 ,"Low Conga"},
{200, 65 ,"High Timbale"},
{200, 66 ,"Low Timbale"},
{200, 67 ,"High Agogo68 Low Agogo"},
{200, 69 ,"Cabasa"},
{200, 70 ,"Maracas"},
{200, 71 ,"Short Whistle"},
{200, 72 ,"Long Whistle"},
{200, 73 ,"Short Guiro"},
{200, 74 ,"Long Guiro"},
{200, 75 ,"Claves"},
{200, 76 ,"Hi Wood Block"},
{200, 77 ,"Low Wood Block"},
{200, 78 ,"Mute Cuica"},
{200, 79 ,"Open Cuica"},
{200, 80 ,"Mute Triangle"},
{200, 81 ,"Open Triangle"}
};
