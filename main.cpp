
//#include <ao/ao.h>
#include "ao.hpp"
#include "midiparse.hpp"
#include "midiplayer.hpp"
#include "wave.hpp"

#include <chrono>
#include <sched.h>
#include <unistd.h>

using milli = std::chrono::duration<double, std::milli>;
std::chrono::steady_clock::time_point initTime;
void                                  ResetTimer() { initTime = std::chrono::steady_clock::now(); };
// return time elapsed in milliseconds
double GetTimeElapsed()
{
	auto now = std::chrono::steady_clock::now();
	return milli(now - initTime).count();
}

void setscheduler(void)
{
	struct sched_param sched_param;
	if (sched_getparam(0, &sched_param) < 0) {
		printf("Scheduler getparam failed...\n");
		return;
	}
	sched_param.sched_priority = sched_get_priority_max(SCHED_RR);
	if (!sched_setscheduler(0, SCHED_RR, &sched_param)) {
		printf("Scheduler set to Round Robin with priority %i...\n", 
			       	sched_param.sched_priority);
		fflush(stdout);
		return;
	}
	printf("Scheduler set to Round Robin with priority %i FAILED!\n",
	       sched_param.sched_priority);
}

class songstream : public ao::streamCB_t {
    public:
	song_t&                         song;
	player_t                        player;
	std::vector<track_t*>::iterator t;

	int texec;

	songstream(song_t& s)
	    : song(s)
	    , player()
	{
		t     = song.tracks.begin();
		texec = 0;

		player.Init(&song);
	};
	virtual int Callback(float* in, int in_s)
	{
		if (t != song.tracks.end()) {
			if (!player.Step(in, in_s))
				t++;
		} else {
			return 0;
		}
		texec++;
		return 1;
	}
};

int main(int argc, char* argv[])
{
	ao::init();
	if (argc < 2) {
		fprintf(stderr,"require target file name\n");
		return -1;
	}

	song_t song;
	if (!ParseMidi(argv[1], song)) {
		fprintf(stderr, "failed to parse midi file\n");
		return -1;
	}

	songstream cb(song);

	song.dumpDebug();

	if (argc >= 3) {
		// save to file

		const char*    filename      = argv[2];
		const int      format        = 3;
		const int      sample_rate   = 44100;
		const int      numchnls      = 1;
		const int      bitsPerSample = 32;
		unsigned char* data          = new unsigned char[song.length() * sizeof(float)];

		for (int i = 0; i < song.length() - 512; i += 512) {
			cb.Callback(((float*)data) + i, 512);
		}

		int rc = saveWave(filename, format, sample_rate, numchnls, bitsPerSample, data,
		                  song.length());
		if(rc < 1)
			fprintf(stderr, "failed to save wave file\n");
		return 0;
	}

	ao::init();
	ao::device_t dev;
	ao::stream_t stream;

	if (!ao::open_device(dev)) {
		fprintf(stderr,"failed to open device\n");
		return -1;
	}
	if (!ao::open_stream(dev, stream, cb)) {
		fprintf(stderr, "failed to open stream\n");
	} else {
		printf("OPEN\n");
	}

	// wait for song to
	// finish playing
	while (ao::state_stream(dev, stream)) {
		sleep(1);
	}

	ao::close_stream(dev, stream);
	ao::close_device(dev);
	ao::quit();

	printf("Exec %d\n", cb.texec);
	return 0;
};
