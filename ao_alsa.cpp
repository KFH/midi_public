#include "ao.hpp"
#include <algorithm>
#include <alsa/asoundlib.h>
#include <thread>

namespace ao {

snd_output_t* output_ptr = nullptr;
static_assert(sizeof(device_t) >= sizeof(snd_pcm_t*), "DEVICE SIZE MISMATCH");

int init()
{
	// TODO init any necessary global here
	// maybe start a thread just for sound ?
	snd_output_stdio_attach(&output_ptr, stdout, 0);

	return 1;
}
int open_device(device_t& dev)
{
	printf("OPEN DEVICE\n");

	dev = 0;
	snd_pcm_t*           handle;
	snd_pcm_hw_params_t* hwparams = nullptr;
	snd_pcm_sw_params_t* swparams = nullptr;
	snd_pcm_hw_params_alloca(&hwparams);
	snd_pcm_sw_params_alloca(&swparams);
	snd_pcm_sframes_t      frames;
	const snd_pcm_access_t accesst   = SND_PCM_ACCESS_RW_INTERLEAVED;
	const snd_pcm_format_t formatt   = SND_PCM_FORMAT_FLOAT_LE;
	const int              latency   = 512; // 8192 * 2;
	const int              rate      = 44100;
	int                    pollcount = 0;

	if (snd_pcm_open(&handle, "pulse", SND_PCM_STREAM_PLAYBACK, SND_PCM_NONBLOCK) < 0) {
		fprintf(stderr, "failed to open PCM stream\n");
		return 0;
	} else {
		printf("opened \"%s\"\n", snd_pcm_name(handle));
	}

	dev = handle;

	// set_hwparams(handle, hwparams, SND_PCM_ACCESS_RW_INTERLEAVED);
	// set_swparams(handle, swparams);
	/*if (snd_pcm_set_params(handle, formatt, accesst, 1, rate, 0, latency * 2) < 0) {
	 fprintf(stderr, "failed to set PCM params\n");
	 return 0;
	}*/

	snd_pcm_uframes_t periodSizeInFrames = 0;
	snd_pcm_uframes_t period_size_target = 1024;
	unsigned int      periodLen          = period_size_target * 16;
	unsigned int      bufferLen          = period_size_target * 8;
	unsigned int      periods            = 0;
	int               dir                = 1;
	int               err                = 0;
	err                                  = snd_pcm_hw_params_any(handle, hwparams);
	if (err < 0) {
		fprintf(stderr, "failed snd_pcm_hw_params_any: %s\n", snd_strerror(err));
		return 0;
	}
	/* set hardware resampling */
	err = snd_pcm_hw_params_set_rate_resample(handle, hwparams, 1);
	if (err < 0) {
		fprintf(stderr, "failed snd_pcm_hw_params_set_rate_resample: %s\n",
		        snd_strerror(err));
		return 0;
	}

	err = snd_pcm_hw_params_set_rate(handle, hwparams, rate, 1);
	if (err < 0) {
		fprintf(stderr, "failed snd_pcm_hw_params_set_rate: %s\n", snd_strerror(err));
		return 0;
	}

	err = snd_pcm_hw_params_set_access(handle, hwparams, accesst);
	if (err < 0) {
		fprintf(stderr, "failed snd_pcm_hw_params_set_access: %s\n", snd_strerror(err));
		return 0;
	} else {
		snd_pcm_access_t at = SND_PCM_ACCESS_MMAP_COMPLEX;
		snd_pcm_hw_params_get_access(hwparams, &at);
		printf("set pcm access %d req %d\n", at, accesst);
	}

	err = snd_pcm_hw_params_set_format(handle, hwparams, formatt);
	if (err < 0) {
		fprintf(stderr, "failed snd_pcm_hw_params_set_format: %s\n", snd_strerror(err));
		return 0;
	}

	err = snd_pcm_hw_params_set_channels(handle, hwparams, 1);
	if (err < 0) {
		fprintf(stderr, "failed snd_pcm_hw_params_set_channels: %s\n", snd_strerror(err));
		return 0;
	}

	err = snd_pcm_hw_params_set_buffer_time_near(handle, hwparams, &bufferLen, &dir);
	if (err < 0) {
		fprintf(stderr, "failed snd_pcm_hw_params_set_buffer_time_near: %s\n",
		        snd_strerror(err));
		// return 0;
	}

	/*
	err = snd_pcm_hw_params_set_period_size(handle, hwparams, period_size_target, dir);
	if (err < 0) {
	 fprintf(stderr, "failed snd_pcm_hw_params_set_period_size: %s\n",
	         snd_strerror(err));
	}
	*/

	err = snd_pcm_hw_params_set_buffer_size(handle, hwparams, period_size_target * 2);
	if (err < 0) {
		fprintf(stderr, "failed snd_pcm_hw_params_set_buffer_size: %s\n",
		        snd_strerror(err));
	}

	err = snd_pcm_hw_params_set_period_time_near(handle, hwparams, &periodLen, &dir);
	if (err < 0) {
		fprintf(stderr, "failed snd_pcm_hw_params_set_period_time_near: %s\n",
		        snd_strerror(err));
		return 0;
	}

	err = snd_pcm_hw_params_get_period_size(hwparams, &periodSizeInFrames, &dir);
	if (err = snd_pcm_hw_params_get_periods(hwparams, &periods, &dir) < 0) {
		fprintf(stderr, "failed snd_pcm_hw_params_get_periods: %s\n", snd_strerror(err));
	}

	// write params to device
	err = snd_pcm_hw_params(handle, hwparams);
	if (err < 0) {
		printf("failed to set hw params for playback: %s\n", snd_strerror(err));
		return 0;
	}

	printf("hwparams periodSizeInFrames %lu periods %u dir %d\n", periodSizeInFrames, periods,
	       dir);

	snd_pcm_uframes_t bufsize    = 0;
	snd_pcm_uframes_t periodsize = 0;
	snd_pcm_get_params(handle, &bufsize, &periodsize);
	printf("pcm ring buffer %lu period %lu\n", bufsize, periodsize);

	snd_pcm_sw_params_current(handle, swparams);
	snd_pcm_sw_params_set_avail_min(handle, swparams, periodsize);
	snd_pcm_sw_params_set_start_threshold(handle, swparams, periodsize);
	snd_pcm_sw_params(handle, swparams);

	// snd_pcm_hw_params_free(hwparams);
	// snd_pcm_sw_params_free(swparams);

	snd_pcm_prepare(handle);

	snd_pcm_dump(handle, output_ptr);

	return 1;
}

const int temp_buff_size = 1024 * 8;
float     temp_buff[temp_buff_size];

void cb_wrapper(snd_async_handler_t* async)
{
	snd_pcm_t*  pcm     = snd_async_handler_get_pcm(async);
	void*       usr_ptr = snd_async_handler_get_callback_private(async);
	float*      out     = nullptr;
	streamCB_t* cb      = (streamCB_t*)usr_ptr;
	int         avail; // snd_pcm_sframes_t

	printf("CB EXEC\n");
	avail = snd_pcm_avail_update(pcm);
	while (avail >= 512) {

		int n = std::min(temp_buff_size, avail);

		if (cb->Callback(out, n)) {
			// return normally
			snd_pcm_writei(pcm, temp_buff, n);
			avail = snd_pcm_avail_update(pcm);
		} else {
			printf("STREAM EXITING\n");
			snd_pcm_drop(pcm);
		}
	}
	if (avail == -EPIPE) {
		printf("ALSA UNDER RUN\n");
		snd_pcm_prepare(pcm);
	}
}
// alsa support of async callback is fucked
int broken_open_stream(device_t& dev, stream_t& stream, streamCB_t& cb)
{
	printf("OPENING STREAM\n");
	snd_pcm_t*           pcm = (snd_pcm_t*)dev;
	snd_async_handler_t* async;
	snd_async_add_pcm_handler(&async, pcm, cb_wrapper, &cb);
	snd_pcm_start(pcm);
	stream = async;
	return 1;
}
int broken_state_stream(device_t dev, stream_t& stream)
{
	snd_pcm_t*      pcm   = (snd_pcm_t*)dev;
	snd_pcm_state_t state = snd_pcm_state(pcm);
	printf("PCM STATE %d\n", state);
	return state == SND_PCM_STATE_RUNNING;
}
int broken_close_stream(device_t& dev, stream_t& stream)
{
	snd_pcm_t*           pcm   = (snd_pcm_t*)dev;
	snd_async_handler_t* async = (snd_async_handler_t*)stream;
	snd_pcm_drop(pcm);
	snd_async_del_handler(async);
	return 1;
}

class thread_t_ {
    public:
	streamCB_t& cb;
	snd_pcm_t*  pcm;
	bool        doRun;
	std::thread t;

	void run()
	{
		int                  avail;
		int                  threshold     = 1024;
		snd_pcm_uframes_t    pcm_buffer_si = 1024;
		snd_pcm_hw_params_t* params;
		snd_pcm_hw_params_alloca(&params);
		int err = snd_pcm_hw_params_current(pcm, params);
		if (err < 0) {
			fprintf(stderr, "failed snd_pcm_hw_params_current: %s", snd_strerror(err));
		} else {
			err = snd_pcm_hw_params_get_buffer_size(params, &pcm_buffer_si);
			if (err < 0) {
				fprintf(stderr, "failed snd_pcm_hw_params_get_buffer_size: %s",
				        snd_strerror(err));
			} else {
				threshold = pcm_buffer_si / 2;
			}
		}

		while (doRun) {
			avail = snd_pcm_avail_update(pcm);
			while (avail >= threshold) {

				int n = std::min(temp_buff_size, avail);

				if (cb.Callback(temp_buff, n)) {
					snd_pcm_writei(pcm, temp_buff, n);
					avail = snd_pcm_avail_update(pcm);
				} else {
					printf("STREAM EXITING\n");
					snd_pcm_drop(pcm);
					doRun = false;
					return;
				}
			}
			if (avail == -EPIPE) {
				printf("ALSA UNDER RUN\n");
				snd_pcm_prepare(pcm);
			}
			std::this_thread::yield();
		}
	}
	thread_t_(streamCB_t& mcb, snd_pcm_t* mpcm)
	    : cb(mcb)
	    , pcm(mpcm)
	    , doRun(true)
	    , t([&] { run(); })
	{
	}
};

// TODO
// make thread busy wait on
int open_stream(device_t& dev, stream_t& stream, streamCB_t& cb)
{

	stream = new thread_t_(cb, (snd_pcm_t*)dev);

	return 1;
}
int state_stream(device_t dev, stream_t& stream) { return ((thread_t_*)stream)->doRun; }
int close_stream(device_t& dev, stream_t& stream)
{
	thread_t_* mythread = ((thread_t_*)stream);
	mythread->doRun     = false;
	mythread->t.join();

	return 1;
}

int close_device(device_t& dev)
{
	snd_pcm_t* pcm = (snd_pcm_t*)dev;
	snd_pcm_close(pcm);
	return 1;
}
int quit() { return 1; }
} // namespace ao
