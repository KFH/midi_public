#pragma once
#include <algorithm>
#include <emmintrin.h>
#include <math.h>
#include <stdint.h>
#include <vector>
#include <array>
#include <deque>

#define MAX(a, b) ((a) > (b) ? a : b)
#define MIN(a, b) ((a) < (b) ? a : b)
#define MAXCH 16
#define SAMPLES 44100.0
#define DEFAULTNOTELEN 44100.0 / 4.0
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

// NOTES
// 16 MIDI channels.
// Each channel can play a variable number of voices (polyphony). 
// Each channel can play a different instrument (timbre).
// Key-based Percussion is always on channel 10.

// CONTROLLER
// 1 Modulation
// 7 Volume
// 10 Pan
// 11 Expression
// 64 Sustain
// 121 Reset All Controllers
// 123 All Notes Off


// make a psuedorandom float -1.f to +1.f
inline float randf() { return (((float)rand()) / (float)RAND_MAX - 0.5f) * 2.f; }

// make psuedorandom float 0.f to 1.f
inline float randposf() { return (((float)rand()) / (float)RAND_MAX); }

inline uint32_t swapBits(uint32_t num)
{
	return ((num & 0xff000000) >> 24) | ((num & 0x00ff0000) >> 8) | ((num & 0x0000ff00) << 8)
	    | (num << 24);
}

inline uint16_t swapBits(uint16_t num) { return ((num & 0xff00) >> 8) | (num << 8); }

// http://subsynth.sourceforge.net/midinote2freq.html
inline float noteToFreq(uint8_t k) { return (440.f / 32.f) * pow(2.f, ((float)k - 9.f) / 12.f); }

struct osc_t {
	float tspeed;
	float tamp;
	float camp;
	float tfreq; // 261.6f for middle c
	float cfreq;
	float period;
	float offset;

	osc_t(float amp, float freq, float speed)
	{
		tspeed = speed;
		tamp   = amp;
		camp   = 0.f;
		tfreq  = freq;
		cfreq  = freq;
		period = 0.f;
		offset = randposf() * M_PI;
	}

	inline void Addi(float* out)
	{
		cfreq   = cfreq * (1.f - tspeed) + tfreq * tspeed;
		camp    = camp * (1.f - tspeed) + tamp * tspeed;
		float m = 2.f * M_PI * cfreq / SAMPLES;
		out[0] += (sin(period + m)) * camp;
		period += m;
		if (period > 2.f * M_PI) {
			period -= 2.f * M_PI;
		}
	}
	inline void Add(float* out)
	{
		cfreq   = cfreq * (1.f - tspeed) + tfreq * tspeed;
		camp    = camp * (1.f - tspeed) + tamp * tspeed;
		float m = 2.f * M_PI * cfreq / SAMPLES;
		out[0] += (sin(period + m)) * camp;
		out[1] += (sin(period + m * 2.f)) * camp;
		out[2] += (sin(period + m * 3.f)) * camp;
		out[3] += (sin(period + m * 4.f)) * camp;
		period += m * 4.f;
		if (period > 2.f * M_PI) {
			period -= 2.f * M_PI;
		}
	}
	inline void Mult(float* out)
	{
		cfreq   = cfreq * (1.f - tspeed) + tfreq * tspeed;
		float m = 2.f * M_PI * cfreq / SAMPLES;
		out[0] *= (sin(period + m)) * camp + offset;
		out[1] *= (sin(period + m * 2.f)) * camp + offset;
		out[2] *= (sin(period + m * 3.f)) * camp + offset;
		out[3] *= (sin(period + m * 4.f)) * camp + offset;
		period += m * 4.f;
		if (period > 2.f * M_PI) {
			period -= 2.f * M_PI;
		}
	}
};

template <int64_t max> struct delay_t {
	int64_t   len  = max;
	int64_t   h    = 0;
	float gain = 0.80;
	float lowpass = 0.95;
	float vals[max];


	float prev = 0.0;

	void reset() { std::fill_n(vals, max, 0.f); }

	float update(float npos)
	{
		vals[h] = npos;
		h       = (h + 1) % max;
		return vals[h] * gain + npos;
	}
	float reverbe(float npos)
	{
		//ms->c * (out[i] - ms->o);
		float nv = vals[h] + lowpass * (prev-vals[h]);
		prev = nv;
		float out = vals[h] = nv * gain + npos;
		h                   = (h + 1) % max;
		return out;
	}
	void Filter(float in[4])
	{
		in[0] = reverbe(in[0]);
		in[1] = reverbe(in[1]);
		in[2] = reverbe(in[2]);
		in[3] = reverbe(in[3]);
	}
};

struct note_t {
	int64_t start;
	int64_t end;
	uint8_t  key;
	uint8_t  velIn;
	uint8_t  velOut;

	int64_t length() const {
		return end - start;
	}
};

struct lytic_t {
	int64_t start;
	std::string msg;
};

class channel_t {
	public:
	std::vector<note_t> stack;
	std::vector<int64_t>    unresolved;

	void StartNote(uint8_t key, uint8_t vel, uint32_t time)
	{
		unresolved.push_back(stack.size());
		stack.push_back(note_t{ time, time + DEFAULTNOTELEN, key, vel, 0 });
	};
	void EndNote(uint8_t key, uint8_t vel, uint32_t time)
	{
		for (int64_t h = 0; h < unresolved.size(); h++) {
			int64_t i = unresolved[h];
			if (stack[i].key == key) {
				stack[i].end    = time;
				stack[i].velOut = vel;
				unresolved.erase(unresolved.begin() + h);
				break;
			}
		}
	};
	uint32_t length()
	{
		uint32_t end = 0;
		for (note_t n : stack) {
			end = n.end > end ? n.end : end;
		}
		return end;
	}
	uint32_t begin()
	{
		if(stack.size() > 0)
			return stack.front().start;
		return 0;
	}
};

class track_t {
    public:
	channel_t ch[MAXCH];
	uint32_t  start;
	uint32_t  end;
	std::string name;

	std::deque<lytic_t> lyrics;

	uint32_t  CalcLength()
	{
		start = UINT32_MAX;
		end = 0;
		for (channel_t& c : ch) {
			uint32_t e = c.length();
			uint32_t s = c.begin();
			end        = std::max(e,end);
			if(e > 0)
				start      = std::min(start,std::min(s,end)); 
		}
		if(start == UINT32_MAX)
			start = 0;
		return end;
	}
};

class song_t {
    public:
	std::vector<track_t*> tracks;
	uint32_t              length() const
	{
		uint32_t e = 0;
		for (auto& t : tracks) {
			e = std::max(e, t->end);
		}
		return e;
	}

	void dumpDebug() const {
#ifdef DEBUG
		printf("SONG:\n	%.02fsec\n", float(length()) / float(SAMPLES));

		for(auto t : tracks) {

			t->CalcLength();
			printf("TRACK:	%.02fstart	%.02fend\n", 
				float(t->start)/float(SAMPLES), float(t->end) / float(SAMPLES));

			for(int i = 0; i < MAXCH; i++) {
				printf("	CH %d	start %.02fs	end %.02fs %s\n", 
					i,
					t->ch[i].begin() / float(SAMPLES),
					t->ch[i].length() / float(SAMPLES),
					t->name.c_str()
					);
			}
		}
#endif
	}
};

typedef std::array<int64_t, MAXCH> ch_marker_t;

class player_t {
    public:
	int64_t                ctime;

	int64_t            chmark[MAXCH];
	const track_t* track;

	struct voice_t {
		int64_t    channel;
		note_t note;
		osc_t  osc;
	};

	bool display_lytics = false;

	song_t *song;
	std::vector<std::pair<track_t*, ch_marker_t>> chmarkers;
	std::vector<voice_t> actv;

	delay_t<4096> df;

	player_t()
	{
		ctime = 0;
		track = NULL;
		df.reset();
	}


	void Init(song_t* s) {
		song = s;
		ctime = 0;
		for(auto& t : song->tracks) {
			chmarkers.push_back(std::make_pair(t,ch_marker_t{}));
		}
		for(auto t : song->tracks) {
			// adding arbitrary offset to key to ensure drum track is audible
			for(auto &note : t->ch[10].stack) {
				note.key = note.key + 30;
			}
		}
	}


	// input must be multiple of 4 in size
	int64_t Step(float* in, int64_t in_s) //const track_t& t)
	{

		std::fill_n(in, in_s, 0.f);

		//if (&t != track) {
		//	track = &t;
		//	ctime = 0;
		//	std::fill_n(chmark, MAXCH, 0);
		//}


		int64_t end = ctime + in_s;
		
		for(auto &pair : chmarkers) {
			const auto& t = *pair.first;
			auto& chmark = pair.second;
			for (int64_t h = MAXCH - 1; h >= 0; h--) {
				int64_t& i = chmark[h];
				while (i < t.ch[h].stack.size() && t.ch[h].stack[i].start < end) {
					actv.push_back(
							voice_t{ h, t.ch[h].stack[i],
							osc_t(0.06f, 
								noteToFreq(t.ch[h].stack[i].key), 
								0.01f) });
#ifdef DEBUG
					auto& note = t.ch[h].stack[i];
					printf("ch%d	%hhu %0.1fms	%s\n", 
						h, 
						note.key, 
						float(note.length()) / float(SAMPLES) * 1000.f,
					     	t.name.c_str());
#endif
					i++;
				}
			}
		}

		auto applyOsc = [&](osc_t& osc, int64_t s, int64_t e) {
			int64_t sa = ((s + 3) / 4) * 4; // ceil
			int64_t ea = (e / 4) * 4; // floor

			for (int64_t i = s; i < sa; i++) {
				osc.Addi(in + i);
			}
			for (int64_t i = sa; i < ea; i += 4) {
				osc.Add(in + i);
			}
			for (int64_t i = ea; i < e; i++) {
				osc.Addi(in + i);
			}
		};
		const float minvol = 0.000001f;
		for (auto& v : actv) {
			int64_t e = std::min(v.note.end, end) - ctime;
			int64_t s = std::min(end,std::max(v.note.start, ctime)) - ctime;
			applyOsc(v.osc, s, e);

			// TODO
			// fall off if past notes end but still resonating
			// needs better design
			if (v.note.end <= end && v.osc.camp > minvol) {
				v.osc.tamp = 0.f;
				s          = std::max(v.note.end, ctime);
				e          = std::min(int64_t(s + SAMPLES), end) - ctime;
				s -= ctime;
				applyOsc(v.osc, s, e);
			}
		}

		//  delay/reverb filter test
		for (int64_t i = 0; i < in_s; i += 4) {
			df.Filter(in + i);
		}

		// deactivate unneeded voices
		for (int64_t h = actv.size() - 1; h >= 0; h--) {
			if (actv[h].note.end <= end && actv[h].osc.camp <= minvol) {
				actv.erase(actv.begin() + h);
			}
		}
		ctime += in_s;
		return ctime < song->length();
	}
};

// repack floats into 16 bit ints
class quantize {

	static inline void convert(const __m128 mult, const __m128& a, __m128i& b)
	{
		__m128 result = _mm_mul_ps(a, mult);
		b             = _mm_cvttps_epi32(result);
	}

    public:
	// input must be 16 byte aligned and divisible
	static void Mono(float* idata, int16_t* odata, int64_t in_s)
	{
		// TODO patch up firt-last 4 or 8 floats to avoid out of bounds or unaligned 
		// memory access

		__m128i* out = (__m128i*)odata;
		__m128*  in  = (__m128*)idata;

		const __m128 mult = _mm_set1_ps(32767.f);
		for (size_t i = 0; i < in_s - 8; i += 8) {

			__m128i lo;
			__m128i hi;

			convert(mult, *in, lo);
			in++;
			convert(mult, *in, hi);
			in++;

			__m128i res = _mm_packs_epi32(lo, hi);
			_mm_store_si128(out, res);
			out++;
		}
	}
};

class simpleplayer_t {
    public:
	player_t player;
	song_t*  song;
	track_t* ctrack;
	simpleplayer_t(song_t* s)
	{
		song   = s;
		ctrack = *s->tracks.begin();
	};

	int64_t play(void* in, int64_t in_s)
	{

		while (ctrack != (*song->tracks.end())) {
			track_t* t = ctrack;
			while (player.ctime < t->end) {
				player.Step((float*)in, in_s);
				quantize::Mono((float*)in, (int16_t*)in, in_s);
				goto escape;
			}
		}
	escape:
		return 0;
	}
};
